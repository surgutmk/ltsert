<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Проект</title>
    <!-- <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> -->
    <link href="/css/vendor.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">


</head>
<body>

@include('components.breadcrumbs')
<div class="project">
    <div class="container ">

        <div class="row client project__client d-flex align-items-baseline">
            <div class="col-md-2 text-gray">Клиент</div>
            <div class="col-md-8"><h3 class="mb-0">ООО «Море волнуется раз»</h3></div>
            <div class="col-md-2 text-right"><a href="#"><h4 class="mb-0 text-primary-light">www.mvr.ru</h4></a></div>
        </div>

        <div class="row">
            <div class="col-md-1 project__splitter"><hr></div>
        </div>

        <div class="row">
            <div class="col-md-2 text-gray">Цели клиента</div>
            <div class="col-md-7">
                <ul class="project__list">
                    <li class="h4 list__item">Цель 1. Описание цели номер один</li>
                    <li class="h4 list__item">Цель 2. Описание цели номер два</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-1 project__splitter"><hr></div>
        </div>

        <div class="row">
            <div class="col-md-2 text-gray">Задачи ЛТС <br> Групп</div>
            <div class="col-md-7">
                <ul class="project__list">
                    <li class="h4 list__item">Задача 1. Описание задачи номер один</li>
                    <li class="h4 list__item">Задача 2. Описание задачи номер два</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-1 project__splitter"><hr></div>
        </div>

        <div class="row">
            <div class="col-md-2 text-gray">Реализация</div>
            <div class="col-md-9">
                <p class="project__header">Шаг 1</p>
                <p>В некотором царстве, в некотором государстве жил да был царь с царицею, у него было три сына — все молодые, холостые, удальцы такие, что ни в  сказке сказать, ни пером написать; младшего звали Иван-царевич.</p>
                    
                <p>Говорит им царь таково слово:</p>
                    
               <p>«Дети мои милые, возьмите себе по стрелке, натяните тугие луки и пустите в разные стороны; на чей двор стрела упадет, там и сватайтесь».</p>
                
                <p>Пустил стрелу старший брат — упала она на боярский двор, прямо против девичья терема; пустил средний брат — полетела стрела к купцу на двор и остановилась у красного крыльца, а на там крыльце стояла душа-девица, дочь купеческая, пустил младший брат — попала стрела в грязное болото, и подхватила её лягуша-квакуша.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-1 project__splitter"><hr></div>
        </div>

        <div class="row">
            <div class="col-md-2 text-gray">Итог</div>
            <div class="col-md-9">
                <p class="project__header">Вывод 1</p>  
                <p>Иван-царевич пошел в дом Кощея, взял Василису Премудрую и воротился домой. После того они жили вместе и долго и счастливо. Вот и сказке Царевна-лягушка конец, а кто слушал - молодец!</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-1 project__splitter"><hr></div>
        </div>

        <div class="row">
            <div class="col-md-2 text-gray">Отзыв клиента</div>
            <div class="col-md-10">
                <div class="row no-gutters project__review">
                        <div class="col-5 review__image">
                            <img src="/images/project/review.jpg" alt="review">
                            <div class="review__gradient"></div>
                            <div class="review__date">08.11.2019</div>
                        </div>
                        <div class="col-7">
                            <div class="review__text">За время работы «ЛенТехСертификация» зарекомендовала себя как компетентная, ответственная и исполнительная компания. Работы по сертификации выполнялись качественно и в согласованные сроки.</div>
                            <div class="review__stars text-center">
                                <ion-icon name="star"></ion-icon>
                                <ion-icon name="star"></ion-icon>
                                <ion-icon name="star"></ion-icon>
                                <ion-icon name="star"></ion-icon>
                                <ion-icon name="star"></ion-icon>
                            </div>
                            <div class="review__autor">
                                <p><b>Дядька черномор,</b><br>
                                директор ООО «Море волнуется раз»</p>
                            </div>
                        </div>
                </div>   
            </div>
        </div>

    </div>
</div>
@include('components.footer')





<script src="/js/app.js"></script>
<script src="https://unpkg.com/ionicons@4.0.0/dist/ionicons.js"></script>
</body>
</html>