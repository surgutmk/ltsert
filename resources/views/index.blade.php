<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Главная</title>
    <link href="/css/vendor.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
</head>
<body>
<div class="entry-block index__entry-block">
    <div class="container position-relative">
        <div class="swiper-container"
             data-prev="#entrySliderPrev"
             data-next="#entrySliderNext"
        >
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-12 col-md-5">
                            <h2 class="text-primary-light">Сертификат Морского и Речного регистров</h2>
                            <div class="h5 mb-6">Предлагаем провести испытания и оформить сертификаты морского и речного
                                регистра
                                судоходства
                            </div>
                            <a href="#" class="btn btn_secondary">Перейти в раздел</a>
                        </div>
                        <div class="col-12 col-md-7 text-center">
                            <img class="index__entry-block-img" src="/images/index/certificate.png">
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-12 col-md-5">
                            <h2 class="text-primary-light">Сертификат Морского и Речного регистров</h2>
                            <div class="h5 mb-6">Предлагаем провести испытания и оформить сертификаты морского и речного
                                регистра
                                судоходства
                            </div>
                            <a href="#" class="btn btn_secondary">Перейти в раздел</a>
                        </div>
                        <div class="col-12 col-md-7 text-center">
                            <img class="index__entry-block-img" src="/images/index/certificate.png">
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-12 col-md-5">
                            <h2 class="text-primary-light">Сертификат Морского и Речного регистров</h2>
                            <div class="h5 mb-6">Предлагаем провести испытания и оформить сертификаты морского и речного
                                регистра
                                судоходства
                            </div>
                            <a href="#" class="btn btn_secondary">Перейти в раздел</a>
                        </div>
                        <div class="col-12 col-md-7 text-center">
                            <img class="index__entry-block-img" src="/images/index/certificate.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="slider-controls entry-block__slider-controls">
            <button class="btn btn_primary slider-controls__prev" id="entrySliderPrev"></button>
            <button class="btn btn_primary slider-controls__next" id="entrySliderNext"></button>
        </div>
    </div>
</div>
@include('components.services')
@component('components.callback')
    @slot('h5')
        Без паники! Оставьте номер телефона, и засекайте время.<br>Решение уже близко.
    @endslot
@endcomponent
@include('components.promo-cert')
@include('components.advantages')
<div class="mt-n6"></div>
@component('components.callback')
    @slot('h4')
        «Все звучит очень серьезно, но мне нужно подумать».
    @endslot
    @slot('h5')
        Если, вдруг, вы так думаете, позвольте позвонить вам и<br/>развеять ваши опасения.
    @endslot
@endcomponent
@include('components.object-selector')
@include('components.certificates')
@include('components.callback-primary')
@include('components.director')
@include('components.clients')
@include('components.blog')
@include('components.feedback')
<script src="/js/app.js"></script>
</body>
</html>

