<div class="director-block">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-5 d-flex align-items-start justify-content-end">
                <img src="/images/index/director.jpg">
            </div>
            <div class="col-12 col-md-6 offset-md-1">
                <div class="page-block">
                    <div class="h3 mb-6 font-weight-light">Помогая другим — ты по&#8209;настоящему можешь быть
                        счастливым.
                    </div>
                    <div class="d-flex align-items-start justify-content-between">
                        <div>
                            <div class="h3 text-primary-light">
                                Артём Робсон
                            </div>
                            <div class="h5 text-gray">
                                Генеральный директор «ЛТС Групп»
                            </div>
                        </div>
                        <img src="/images/index/director-sign.png"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>