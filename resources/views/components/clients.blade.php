<div class="clients-block page-block">
    <h2 class="text-center mb-6">Наши клиенты</h2>
    <div class="container">
        <div class="swiper-container"
             data-prev="#clientSliderPrev"
             data-next="#clientSliderNext"
        >
            <div class="swiper-wrapper mb-6">
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-6 col-md-3">
                            <div class="client-card">
                                <img class="client-card__image" src="/images/index/partner-tesla.png">
                            </div>
                        </div>
                        <div class="col-6 col-md-3">
                            <div class="client-card">
                                <img class="client-card__image" src="/images/index/partner-spacex.png">
                            </div>
                        </div>
                        <div class="col-6 col-md-3">
                            <div class="client-card">
                                <img class="client-card__image" src="/images/index/partner-nasa.png">
                            </div>
                        </div>
                        <div class="col-6 col-md-3">
                            <div class="client-card">
                                <img class="client-card__image" src="/images/index/partner-tesla.png">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-6 col-md-3">
                            <div class="client-card">
                                <img class="client-card__image" src="/images/index/partner-tesla.png">
                            </div>
                        </div>
                        <div class="col-6 col-md-3">
                            <div class="client-card">
                                <img class="client-card__image" src="/images/index/partner-spacex.png">
                            </div>
                        </div>
                        <div class="col-6 col-md-3">
                            <div class="client-card">
                                <img class="client-card__image" src="/images/index/partner-nasa.png">
                            </div>
                        </div>
                        <div class="col-6 col-md-3">
                            <div class="client-card">
                                <img class="client-card__image" src="/images/index/partner-tesla.png">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-6 col-md-3">
                            <div class="client-card">
                                <img class="client-card__image" src="/images/index/partner-tesla.png">
                            </div>
                        </div>
                        <div class="col-6 col-md-3">
                            <div class="client-card">
                                <img class="client-card__image" src="/images/index/partner-spacex.png">
                            </div>
                        </div>
                        <div class="col-6 col-md-3">
                            <div class="client-card">
                                <img class="client-card__image" src="/images/index/partner-nasa.png">
                            </div>
                        </div>
                        <div class="col-6 col-md-3">
                            <div class="client-card">
                                <img class="client-card__image" src="/images/index/partner-tesla.png">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slider-controls justify-content-center">
                <button class="btn btn_primary slider-controls__prev" id="clientSliderPrev"></button>
                <button class="btn btn_primary slider-controls__next" id="clientSliderNext"></button>
            </div>
        </div>
    </div>
</div>