<div class="page-block services-block container">
    <div class="row">
        <div class="col-12 col-md-8 offset-md-2">
            <div class="text-center text-gray">
                @isset($h4)
                    <div class="h4 font-weight-light mb-2">{{$h4}}</div>
                @endisset
                @isset($h5)
                    <div class="h5 font-weight-light">{{$h5}}</div>
                @endisset
            </div>
            <div class="d-flex">
                <div class="row w-100">
                    <div class="col-6">
                        <div class="input mb-0">
                            @php($id = Str::random(2))
                            <label for="callback_{{$id}}">Куда перезвонить?</label>
                            <input placeholder="Телефон" id="callback_{{$id}}">
                        </div>
                    </div>
                    <div class="col-6">
                        <button class="btn btn_secondary w-100">Жду звонка</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>