<div class="page-block services-block container">
    <h2 class="text-center mb-6">ВСЕ УСЛУГИ</h2>
    <div class="row">
        <div class="col-12 col-md-4">
            <ul class="services-block__nav">
                <li>
                    <a class="h6 services-block__nav-item" href="#">
                        <img src="/images/index/icon-razrabotka-stu.svg"/>Разработка и согласование СТУ
                    </a>
                </li>
                <li>
                    <a class="services-block__nav-item h6" href="#">
                        <img src="/images/index/icon-certification.svg"/>Сертификация
                    </a>
                    <ul class="services-block__sub-nav">
                        <li>
                            <a class="services-block__sub-nav-item" href="#">Сертификат соответствия ТР ТС</a>
                        </li>
                        <li>
                            <a class="services-block__sub-nav-item" href="#">Декларация соответствия ТР ТС</a>
                        </li>
                        <li>
                            <a class="services-block__sub-nav-item" href="#">Сертификат соответствия ГОСТ Р</a>
                        </li>
                        <li>
                            <a class="services-block__sub-nav-item" href="#">Декларация соответствия ГОСТ Р</a>
                        </li>
                        <li>
                            <a class="services-block__sub-nav-item" href="#">Отказное письмо</a>
                        </li>
                        <li>
                            <a class="services-block__sub-nav-item" href="#">Сертификат Морского и Речного регистров</a>
                        </li>
                        <li>
                            <a class="services-block__sub-nav-item" href="#">СЕ Маркировка (Европейский сертификат)</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="services-block__nav-item h6" href="#">
                        <img src="/images/index/icon-fire-certification.svg"/>Добровольный пожарный сертификат
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-12 col-md-4">
            <ul class="services-block__nav">
                <li>
                    <a class="services-block__nav-item h6" href="#">
                        <img src="/images/index/icon-documentation.svg"/>Техническая документация

                    </a>
                    <ul class="services-block__sub-nav">
                        <li>
                            <a class="services-block__sub-nav-item" href="#">Разработка и регистрация ТУ</a>
                        </li>
                        <li>
                            <a class="services-block__sub-nav-item" href="#">Разработка руководства по эксплуатации</a>
                        </li>
                        <li>
                            <a class="services-block__sub-nav-item" href="#">Разработка паспорта безопасности опасного
                                объекта</a>
                        </li>
                        <li>
                            <a class="services-block__sub-nav-item" href="#">Обоснование безопасности оборудования и
                                машин</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="services-block__nav-item h6" href="#">
                        <img src="/images/index/icon-iso.svg"/>ISO (ИСО)

                    </a>
                    <ul class="services-block__sub-nav">
                        <li>
                            <a class="services-block__sub-nav-item" href="#">ISO (ИСО)</a>
                        </li>
                        <li>
                            <a class="services-block__sub-nav-item" href="#">добавить с iso-mss.ru</a>
                        </li>
                        <li>
                            <a class="services-block__sub-nav-item" href="#">Сертификация ХАССП</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="col-12 col-md-4">
            <ul class="services-block__nav">
                <li>
                    <a class="services-block__nav-item h6" href="#">
                        <img src="/images/index/icon-boot.svg"/>Морской и Речной регистры

                    </a>
                    <ul class="services-block__sub-nav">
                        <li>
                            <a class="services-block__sub-nav-item" href="#">Сертификат Морского и Речного регистров</a>
                        </li>
                        <li>
                            <a class="services-block__sub-nav-item" href="#">Испытательная лаборатория РМРС</a>
                        </li>
                        <li>
                            <a class="services-block__sub-nav-item" href="#">Обучение сварщиков в соответствии с
                                требованиями РМРС</a>
                        </li>
                        <li>
                            <a class="services-block__sub-nav-item" href="#">Подряд сварщиков в соответствии с
                                требованиями РМРС</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="services-block__nav-item h6" href="#">
                        <img src="/images/index/icon-school.svg"/>Обучение (учебный центр)
                    </a>
                </li>
                <li>
                    <a class="services-block__nav-item h6" href="#">
                        <img src="/images/index/icon-hammer.svg"/>Для СТУ
                    </a>
                    <ul class="services-block__sub-nav">
                        <li>
                            <a class="services-block__sub-nav-item" href="#">Расчет пожарных рисков</a>
                        </li>
                        <li>
                            <a class="services-block__sub-nav-item" href="#">Пожарные СТУ</a>
                        </li>
                        <li>
                            <a class="services-block__sub-nav-item" href="#">Просто не пожарные СТУ</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>