<div class="callback-primary-block">
    <div class="container">
        <form class="row">
            <div class="col-12 col-md-4">
                <div class="h2 text-center">
                    Запросить <span class="text-secondary">бесплатную</span> консультацию
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="input input_light">
                    <label for="callbackPhone">Куда перезвонить?</label>
                    <input placeholder="Телефон" type="tel" id="callbackPhone">
                </div>
                <div class="input input_light">
                    <label for="callbackEmail">Что писать мы знаем, не знаем пока куда</label>
                    <input placeholder="Электронная почта" type="email" id="callbackEmail">
                </div>
                <div class="input-cb input_light input-cb_sm ml-5">
                    <input type="checkbox" id="callbackTerms">
                    <label for="callbackTerms">
                        Я принимаю условия <a href="#" class="text-white">оферты</a> и
                        <a href="#" class="text-white">пользовательского
                            соглашения</a>
                    </label>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="input input_light">
                    <label for="callbackMsg">Любые мысли по поводу и без. Вдруг станут классикой</label>
                    <textarea placeholder="Комментарий" id="callbackMsg"
                    rows="3"></textarea>
                </div>
                <button class="btn btn_secondary px-0 w-100">Запросить бесплатно</button>
            </div>
        </form>
    </div>
</div>