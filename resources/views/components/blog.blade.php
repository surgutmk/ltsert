<div class="blog-block page-block">
    <div class="container">
        <h2 class="text-center mb-6">Последние статьи в блоге</h2>
        <div class="row mb-5">
            <div class="col-12 col-md-4">
                <div class="article-card">
                    <div class="article-card__img"
                         style="background-image: url('/images/index/article-1.jpg')"
                    >
                        <div class="article-card__date">08.11.2019</div>
                    </div>
                    <div class="article-card__title">
                        Обучение по охране труда: от безопасности сотрудников к соблюдению закона
                    </div>
                    <div class="article-card__text">
                        Обучение по охране труда призвано уберечь персонал от несчастных случаев на рабочем месте,
                        благодаря подробному инструктированию с последующей проверкой полученных знаний. ...
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="article-card">
                    <div class="article-card__img"
                         style="background-image: url('/images/index/article-2.jpg')"
                    >
                        <div class="article-card__date">08.11.2019</div>
                    </div>
                    <div class="article-card__title">
                        Сертификация трансформаторов: нормативные акты, необходимые документы...
                    </div>
                    <div class="article-card__text">
                        Какие нормативные акты действуют в отношении различных видов трансформаторов? Как получить
                        сертификат соответствия на трансформатор тока? Об этом рассказывают специалисты компании...
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="article-card">
                    <div class="article-card__img"
                         style="background-image: url('/images/index/article-3.jpg')"
                    >
                        <div class="article-card__date">08.11.2019</div>
                    </div>
                    <div class="article-card__title">
                        Сертификация кондиционеров: как подтверждается соответствие сплит систем?
                    </div>
                    <div class="article-card__text">
                        Какие нормативные акты в области сертификации кондиционеров актуальны? Как получить сертификат
                        соответствия на кондиционер? Какие документы необходимо собрать для подачи ...
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-4 offset-md-4">
                <a href="#" class="btn btn_primary btn_outline w-100">Перейти в блог</a>
            </div>
        </div>
    </div>
</div>