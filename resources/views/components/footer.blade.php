<div class="footer">
    <div class="container footer__wrapper">
        <div class="row">
            <div class="col-md-3">
                <div class="row h-100">
                    <div class="col-12"><img src="/images/project/logo.png" alt="logo"></div>
                    <div class="col align-self-end"><div class="small">Политика конфиденциальности <br> и обработка данных</div></div>
                </div>
            </div>
            <div class="col-md-3 subtitle-sm font-weight-bold">
                <ul class="footer-list">
                    <li class="footer-list__item"><a class="footer-list__link" href="#">Услуги</a></li>
                    <li class="footer-list__item"><a class="footer-list__link" href="#">Проекты</a></li>
                    <li class="footer-list__item"><a class="footer-list__link" href="#">Вопросы и ответы</a></li>
                    <li class="footer-list__item"><a class="footer-list__link" href="#">О Компании</a></li>
                    <li class="footer-list__item"><a class="footer-list__link" href="#">Партнерам</a></li>
                    <li class="footer-list__item"><a class="footer-list__link" href="#">Блог</a></li>
                    <li class="footer-list__item"><a class="footer-list__link" href="#">Контакты</a></li>
                </ul>
            </div>
            <div class="col-md-2 small">
                <p>Бесплатно по России<br><span class="text-secondary font-weight-bold">8 800 512 00 01</span></p>
                <p>Почта<br><span class="font-weight-bold">admonistration@itsert.ru</span></p>
                <div class="footer-social d-flex justify-content-between">
                    <a class="footer-social__link" href="#"><i class="fab fa-vk"></i></a>
                    <a class="footer-social__link" href=""><i class="fab fa-facebook-f"></i></a>
                    <a class="footer-social__link" href=""><i class="fab fa-youtube"></i></a>
                </div>
            </div>
            <div class="col-md-4 d-flex justify-content-end">
            
                <button class="footer-button small"><i class="fas fa-phone-alt"></i> Заказать звонок</button>
            </div>
        </div>
    </div>
    <div class="footer__dev-link text-center small">
        Разработка и продвижение - <a class="text-secondary" href="#">Net-Simple.agency</a>
    </div>
</div>
