<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="list breadcrumbs__list font-weight-light">
                    <li class="list__item"><a class="list__link" href="#">Главная</a></li>
                    <li class="list__item"><a class="list__link" href="#">Проекты</a></li>
                    <li class="list__item">Реализованный проект</li>
                </ul>
            </div>
        </div>
    </div>
</div>
