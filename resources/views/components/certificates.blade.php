<div class="certificates-block">
    <div class="page-block container">
        <div class="h2 text-center mb-6">
            Аккредитация / Сертификаты
        </div>
        <div class="row mb-5">
            <div class="col-12 col-md-10 offset-md-1 text-center text-primary-light px-md-5">
                Морским регистром осуществляется техническое наблюдение за изготовлением материалов и изделий в
                соответствии
                с требованиями правил РС, международных конвенций и соглашений, рекомендаций Международной морской
                организации и морских администраций.
            </div>
        </div>
        <div class="swiper-container certificates-block__swiper-container"
            data-prev="#certificatesPrev"
            data-next="#certificatesNext"
             data-spacebetween="1000"
        >
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="row align-items-center ">
                        <div class="col-12 col-md-4 offset-md-1">
                            <img src="/images/index/certificate.png" class="certificates-block__img">
                        </div>
                        <div class="col-12 col-md-6 offset-md-1 certificates-block__cert-desc">
                            <div class="h3 text-primary-light mb-4">Свидетельство о соответствии предприятия</div>
                            <p>Данное свидетельство позволяет на оформлять</p>
                            <p>то</p>
                            <p>это</p>
                            <p>и еще вот это</p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row align-items-center">
                        <div class="col-12 col-md-4 offset-md-1">
                            <img src="/images/index/certificate.png" class="certificates-block__img">
                        </div>
                        <div class="col-12 col-md-6 offset-md-1 certificates-block__cert-desc">
                            <div class="h3 text-primary-light mb-4">Свидетельство о соответствии предприятия</div>
                            <p>Данное свидетельство позволяет на оформлять</p>
                            <p>то</p>
                            <p>это</p>
                            <p>и еще вот это</p>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="row align-items-center">
                        <div class="col-12 col-md-4 offset-md-1">
                            <img src="/images/index/certificate.png" class="certificates-block__img">
                        </div>
                        <div class="col-12 col-md-6 offset-md-1 certificates-block__cert-desc">
                            <div class="h3 text-primary-light mb-4">Свидетельство о соответствии предприятия</div>
                            <p>Данное свидетельство позволяет на оформлять</p>
                            <p>то</p>
                            <p>это</p>
                            <p>и еще вот это</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-3 offset-md-6">
                    <div class="slider-controls certificates-block__slider-controls">
                        <button class="btn btn_primary slider-controls__prev" id="certificatesPrev"></button>
                        <button class="btn btn_primary slider-controls__next" id="certificatesNext"></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>