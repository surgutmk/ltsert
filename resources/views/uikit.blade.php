<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>UIKit</title>
    <link href="/css/vendor.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    <style>
        .circle {
            width: 50px;
            height: 50px;
            border-radius: 50%;
            margin-bottom: 15px;
        }

        .circle-wrap{
            display: flex;
            flex-direction: column;
            align-items: center;
            margin: 0 15px 0 15px;
        }
    </style>
</head>
<body class="text-primary-light p-3">
<div class="mb-5">
    <h1>H0 SuperHead 72 Regular</h1>
    <h2>H1 Head 48 Bold</h2>
    <h3>H2 Headline 36 Bold</h3>
    <h4>
        H3 Headline 24 Bold
    </h4>
    <h4 class="font-weight-light">
        H3 Headline 24 Light
    </h4>
    <h5>
        H4 Headline 20 Regular
    </h5>
    <h5 class="font-weight-light">
        H4 Headline 20 Light
    </h5>
    <h5 class="font-weight-bold">
        H4 Headline 20 Bold
    </h5>
    <div class="h6">
        H5 Headline 18 Medium
    </div>
    <div>Body 1 18 Regular</div>
    <div class="subtitle-sm">
        Subtitle 3 14 Light
    </div>
    <div class="small">Body 2 12</div>
    <div class="extra-small">Caption 11</div>
    <div class="small">Overline 12 Regular</div>
    <div class="small font-weight-bold">Overline 12 Bold</div>
</div>
<div class="mb-5">
    <button class="btn btn_primary">Primary Button</button>
    <button class="btn btn_secondary">Secondary Button</button>
</div>
<div class="d-flex flex-wrap">
    <div class="circle-wrap">
        <div class="circle bg-primary"></div>
        <div>Primary</div>
    </div>
    <div class="circle-wrap">
        <div class="circle bg-dark"></div>
        <div>Dark</div>
    </div>
    <div class="circle-wrap">
        <div class="circle bg-primary-light"></div>
        <div>Primary light</div>
    </div>
    <div class="circle-wrap">
        <div class="circle bg-secondary"></div>
        <div>Secondary</div>
    </div>
    <div class="circle-wrap">
        <div class="circle bg-secondary-dark"></div>
        <div>Secondary dark</div>
    </div>
    <div class="circle-wrap">
        <div class="circle bg-secondary-light"></div>
        <div>Secondary light</div>
    </div>
    <div class="circle-wrap">
        <div class="circle bg-gray"></div>
        <div>Gray</div>
    </div>
    <div class="circle-wrap">
        <div class="circle bg-gray-light"></div>
        <div>Gray light</div>
    </div>
    <div class="circle-wrap">
        <div class="circle bg-warning"></div>
        <div>Warning</div>
    </div>
    <div class="circle-wrap">
        <div class="circle bg-danger"></div>
        <div>Danger</div>
    </div>
</div>
</body>
</html>

